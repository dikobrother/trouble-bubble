using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    [SerializeField] private float _enemySpeed;

    private void Update()
    {
        transform.Translate(Vector3.up * _enemySpeed * Time.deltaTime);
    }
}
