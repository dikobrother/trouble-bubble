using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{

    [SerializeField] private int _healthCount;

    public event UnityAction<int> HealthChanged;
    public event UnityAction Died;

    private void Start()
    {
        HealthChanged?.Invoke(_healthCount);
    }
    public void DamageGain (int damage)
    {
        _healthCount -= damage;
        HealthChanged?.Invoke(_healthCount);
        if (_healthCount <= 0)
        {
            Death();
        }
    }

    public void Death()
    {
        Died?.Invoke();
      

    }
}
