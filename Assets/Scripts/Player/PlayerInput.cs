using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerMovement))]

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private Button _leftMoveButton;
    [SerializeField] private Button _rightMoveButton;

    private PlayerMovement _mover;

    private void OnEnable()
    {
        _leftMoveButton.onClick.AddListener(OnLeftMoveButtonClick);
        _rightMoveButton.onClick.AddListener(OnRightMoveButtonClick);
    }

    private void OnDisable()
    {
        _leftMoveButton.onClick.RemoveListener(OnLeftMoveButtonClick);
        _rightMoveButton.onClick.RemoveListener(OnRightMoveButtonClick);
    }

    private void Start()
    {
        _mover = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        //if(Input.GetKeyDown(KeyCode.A))
        //{
        //   _mover.TryMoveLeft();
        //}

        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    _mover.TryMoveRight();
        //}

  
    }

    private void OnLeftMoveButtonClick()
    {
        _mover.TryMoveLeft();
    }

    private void OnRightMoveButtonClick()
    {
        _mover.TryMoveRight();
    }
}
