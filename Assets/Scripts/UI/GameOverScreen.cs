using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(CanvasGroup))]
public class GameOverScreen : MonoBehaviour
{

    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _backToMenuButton;
    [SerializeField] private Player _player;
    [SerializeField] private Button _rightMoveButton;
    [SerializeField] private Button _leftMoveButton;
 

    [SerializeField] private CanvasGroup _gameOverGroup;

    private void OnEnable()
    {
        _player.Died += OnDied;
        _restartButton.onClick.AddListener(OnRestartButtonClick);
        _backToMenuButton.onClick.AddListener(OnMenuButtonClick);

    }

    private void OnDisable()
    {
        _player.Died -= OnDied;
        _restartButton.onClick.RemoveListener(OnRestartButtonClick);
        _backToMenuButton.onClick.RemoveListener(OnMenuButtonClick);
    }
    void Start()
    {
        _gameOverGroup = GetComponent<CanvasGroup>();
        _restartButton.interactable = false;
        _backToMenuButton.interactable = false;

        _gameOverGroup.alpha = 0;
    }
    
    private void OnDied()
    {
        _gameOverGroup.alpha = 1;
        _restartButton.interactable = true;
        _backToMenuButton.interactable = true;
        _leftMoveButton.gameObject.SetActive(false);
        _rightMoveButton.gameObject.SetActive(false);
        Time.timeScale = 0;
    }

    private void OnRestartButtonClick()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    private void OnMenuButtonClick()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

}
