using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenuScreen : MonoBehaviour
{
    [SerializeField] private Button _startGameButton;

    private void OnEnable()
    {
        _startGameButton.onClick.AddListener(OnPlayButtonClick);
    }

    private void OnDisable()
    {
        _startGameButton.onClick.RemoveListener(OnPlayButtonClick); 
    }
   
    
    private void OnPlayButtonClick()
    {
        SceneManager.LoadScene(1);
    }
}
